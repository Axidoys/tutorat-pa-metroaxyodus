# DataStructure
The DataStructure is implemented in dstr.c/h

The aim is to reduce processing time to its fastest.
A hypothesis is that the coster operation in the program is the file operation. Mostly debatable, depends on the :
* hardware : severals type of HDD, SDD, ...
* Storage strategies : physical localisation, ...
* OS : bufferization of files, context of execution, ...
Thus, the strategy could be to read all the files at once and save the only specific header - **position {long, lat}** . It could contain the time interval of the measure but simplification are made : see [README.md](README.md) .

In both (interactive and batch) mode, severals commands can be entered, so the sequence is :
  Running executable => reading all files => saving header in RAM
  Then let the normal command prompt run.

With that strategy, we chose a particular data structure (similar to hashtables) : there are 2 vectors containing all the coordinates (X/Y or long/lat..) sorted and their associate files. Thanks to that, if we search for a particular position (or range : see further) we only have a complexity `=O(log(n))` using the Dichotomy algorithm. Opposed to `=n` with a naive exhaustive research.

![Scheme Data structure](/doc/images/dataStructure1.svg)

What we name "vectors" refer to the c++ [std::vector](https://fr.cppreference.com/w/cpp/container/vector) : dynamic arrays. But it is not implemented in C, so we write the lib vector, documented in [Vector.md](Vector.md) .


Remarks :
* We are thinking about usind multithreading (because file reading is long enough to let the user enter firsts commands).
