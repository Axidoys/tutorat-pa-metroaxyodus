
#### Commit message
We use the [AngularJS convention](https://gist.github.com/stephenparish/9941e89d80e2bc58a153).

#### Code format - a tool
We use clang-format to ensure to have an homogenized code with `.clang-format.txt`

#### Code format - guideline
What clang-format do no verify (we may create a script for that) is naming conventions. So please follow these :
* variables : snake_case (mostly one-word)
* functions : snake_case
* private function : snake_case, begin with an underscore
* typedef : PascalCase
* enum : camelCase begin with prefixe "e_"
* #define or Env-var : UPPERCASE


#### Hooks
Developpers can (or should) use hooks in this project :
* `hooks/commit-msg` : use a .py script for angular convention
* `hooks/pre-commit` : use the wrapper of clang-format from Dequidt's repo

For that you have to place these two files into `.git/hooks/`


#### CI-CD
A CI-CD has been setup, for debian :
* `staticanalyse` : clang-format/tidy
* `build` : test the compilation
* `test` : try running main and unitary tests, with valgrind if necessary
* `report` : POC of : "track execution time for a cmd-sample", [here is the reporting site](http://pa.axel-chemin.fr)

#### Contributors
* Axel CHEMIN (IMA3)
* Yoann LORMEAU (IMA3)
