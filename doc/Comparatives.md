# Comparatives
The aim of this files is to record some test conducted along the project. This is made for making choices between severals algorithms, datastructure and other decisions that impact the quality of the program (time process, memory usage, ...) .


## Rule for reporting

```
#### Test #x
<scope> : <study/aim>
<type of simulation, name .sh and .c if possible>

<context : Hardware/Software>

<raw data with a table>

<conclusion>
[decision]
```

## Tests

#### `Test #1`
Vector : Compare contiguous and scattered strategy (see [Vector.md](Vector.md))

Simulate `i` times the adding of a `x` number of data in a vector without knowing in advance how many (no _reserve(size)). `i = 10` ; `x=20000000` ; see files vector*.c

OS:`Debian10/WSL(W10Pro)` RAM:`8,0Go@2400MHz/1slot` Disk:`SSD Samsung:MZVLB256HAHQ-000L7`. May have other softwares running aside (Atom, Discord, Firefox, ...). Assume that there is no ram&swap.

Times for running tests (`real` used):

|     | min | max | avg | med |
| --- | --- | --- | --- | --- |
| Scattered | 1.356 | 1.379 | 1.367 | 1.367 |
| Contiguous | 2.172 | 2.950 | 2.785 | 2.909 |

Surprisingly the data draw that scattered access is faster, an issue (#3) is raised to understand why it happened.

~~So we chose to use the *contiguous strategy*~~ => must be an error / WIP

In the meanwhile we chose to implement the contiguous strategy, it seems that it would be better to use scattered.

#### `Test #2`

Vector : Compare with and without _reserve (see [Vector.md](Vector.md))

Simulate `i` times the adding of a `x` number of data in a vector with and without using _reserve(). `i = 10` ; `x=20000000` ; see files vector*.c

OS:`Debian10/WSL(W10Pro)` RAM:`8,0Go@2400MHz/1slot` Disk:`SSD Samsung:MZVLB256HAHQ-000L7`. May have other softwares running aside (Atom, Discord, Firefox, ...). Assume that there is no ram&swap.

Times for running tests (`real` used):

|     | min (s) | max (s) | avg (s) | med (s) |
| --- | --- | --- | --- | --- |
| Scattered w/o _reserve() | 1.423 | 1.493 | 1.443 | 1.430 |
| Scattered w/  _reserve() | 1.356 | 1.379 | 1.367 | 1.367 |

|     | min (s) | max (s) | avg (s) | med (s) |
| --- | --- | --- | --- | --- |
| Contiguous w/o _reserve()| 3.992 | 4.179 | 4.037 | 4.010 |
| Contiguous w/  _reserve()| 2.172 | 2.950 | 2.785 | 2.909 |

We see an improvement in using the method _reserve() ! Especially in contiguous where the realloc is coster.

#### `Test #3`
atoi : Compare the time for converting all data from string into int with using if or switch-case. Used for process::line_to_data() .

Simulate `i` times the `x` number of operation (atoi/if) . `i = 3` ; `x=2000000000` ; see files stringConv*.c

OS:`Debian10/WSL(W10Pro)` RAM:`8,0Go@2400MHz/1slot` Disk:`SSD Samsung:MZVLB256HAHQ-000L7`. May have other softwares running aside (Atom, Discord, Firefox, ...). Assume that there is no ram&swap.

Times for running tests (`real` used):

|     | min | max | avg | med |
| --- | --- | --- | --- | --- |
| only if | 4.305 | 4.319 | 4.312 | 4.312 |
| atoi | 29.953 | 30.084 | 30.037 | 30.073 |

Drawing the conclusion that we can't use the dumb method of always reading all the columns.


#### `Test #4`
jump_to_line : Compare the time for jumping to a line in a text file. Used for ...::_jump_to_line() .

Simulate `i` times the reading of the line `y` from `x` files . `i = 2` ; `x=20000` ; `y=17000` ; see files jumpToLine*.c

OS:`Debian10/WSL(W10Pro)` RAM:`8,0Go@2400MHz/1slot` Disk:`SSD Samsung:MZVLB256HAHQ-000L7`. May have other softwares running aside (Atom, Discord, Firefox, ...). Assume that there is no ram&swap.

Times for running tests (`real` used):

|     | average |
| --- | --- |
| 1) fgets | 39.550 |
| 2) fgetc | 1m36 |
| 3) getc | 1m44 |
| 3) fgets + fread | 44.952 |
| 3) fgets + fseek | inf (bug) |

So we let the inline form in process.c with fgets




## Note on analyse/timetest/ dir
These tests files should not have been included in the `master` branch, but after a merge mistakes and the decision to write this md file, we decided to let it as it justify or decision. All sources files is runned by Launch.sh, please go in root before and `make libs` .
