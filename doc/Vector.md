# Vector class
This file refers to the vector_cont.c/h and explain some of vector_scat.c/h .

In fact, first we developed and used vector_scat (for scatered memory strategy), but latency test show up a much poorer execution time with it, so we decided to deprecate this lib and develop the new one : vector_cont (for contiguous memory strategy).

## Source
The class is inspired by [this source](https://gist.github.com/EmilHernvall/953968/0fef1b1f826a8c3d8cfb74b2915f17d2944ec1d0) shared under MIT license.
At the beginning, it could only contains literals strings. We add the handle of any types you want to store (and fix what it implies).

In fact, now this class diverge from (c++) [std::vector](https://fr.cppreference.com/w/cpp/container/vector) because it missed functions wich are useless and `vector_scat` can stores heterogeneous objects .

## Use
You need to use _init before and _free after use !

#### vector_scat (deprecated)
Vectors (here) are meant to store pointers, so you have to pay attention to lifetime of objects. There is two cases :
* Want to pass an object store on the **heap**? Use vector_add()
* Want to pass an object store on the **stack** ? Use vector_add_alloc() or the type-specific ones

With the candid strategy of beginning with 10 and increase by factor 2 the allocated size, the function reserve() is well-advised when possible. For example, if loading 449 elements :
* there would be 6 reallocations
* there would be a lots of unnecessary moves : 1830 (`=10+20+40+80+160+320`)
* at the end, 640 places are allocated : 191 unused, 43% of garbage

#### vector_cont
As we use homogeneous vector now, everything is passed by ref and copied directly in the vector.

The strategy explained above is the same, so here we always use reserve()
