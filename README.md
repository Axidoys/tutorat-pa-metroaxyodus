# Tutorat PA - Metroaxyodus

#### Advanced Programming project about atmospheric measures.

![Readme ban](/doc/images/logo.png)

## Getting started

### Install

Code is in C and we use Makefile for building. Makefiles are easy to use, for example with gcc you just have to run `make` in the root folder.

Then, as we wrote libraries, some files would be present in the folder, including the executable `prgm.out` !

You can use different options with `make`, as `RELEASE=1` (default is debug) or `VERBOSE_THRESHOLD=3` for high verbose.

### Usage

#### Prerequisites :

The program is made for analysing the data of amtospheric sensors placed in Aarhus (Denmark). Data files are needed to use the application.
We cannot distribute this data, which are not opened contrary to this program.
Here is the format used if you want to use your own data :
* All you data files gathered in a single folder
* Data files are :
  * The container is csv (Comma-separated values)
  * With the comma ',' for value separator
  * With a dot '.' for decimal separator
  * First line as tag for columns, then values like :
```
zone,particullate_matter,carbon_monoxide,sulfure_dioxide, nitrogen_dioxide,longitude,latitude,timestamp
103,30,56,71,48,10.109984271163967,56.16459887184971,2014-08-01 00:05:00
```

/!\ Note that in the aim of optimizing the processing, we taken account of the redundancy :
* Each dataset (file) begin at the same time : 2014-08-01 00:05:00
* Each line is a jump of 5 minutes
* The columns names and order are constant
* There is more than a file per localization : each file is a new sensor !
* Each file has the same number of measures (17568 measures)
* Radius used is only 1, 10, 100 and 1000 km for localisation filter
  * the calculation associated is simplified (like at the equator)
All of these specifications cannot be bypassed at this version !

#### Commands
Commands set is the same in both mode, only the way to run the program is different

##### Batch mode
Used to read commands from a txt file, it reads the file line by line and ends at the `EOF` charactere, return everything in the console.

For accessing it, it is (use your favorite terminal) :
```
prgm.out <Data Folder> <Command file>
```

##### Interactiv mode
Used to read user live commands
For accessing it, it is :
```
prgm.out <Data Folder>
```

##### Command syntax
Each command must be structured like this :
```
metric [opt] type [longitude latitude radius]
```

* metric (`log`, `daily`, `monthly` or `global`)
* opt : exist only if log, daily or monthly is used
* type (`ozone`, `particullate_matter`, `carbon_monoxide`, `sulfure_dioxide` or `nitrogen_dioxide`)
* lon, lat and radius : are for localization filtering

### Miscellaneous

#### Contributing
[See more](/doc/Contributing.md)

#### More docs
We write a bunch of md-files to explain how our programs work and our choices thanks to some test and reflexion conducted about our problematic :

* Some comparative tests : [Comparatives.md](/doc/Comparatives.md)
* Our data structure : [DataStructure.md](/doc/DataStructure.md) and [Vector.md](/doc/Vector.md)

#### Last remark
As we are only two developpers, we tends to commit with low verbose, and we used CI-CD more for learning that everything. Moreover, pardon our litle mistakes at us using the git tools (maybe some branches are inapropriate or commit messages poorly written).
