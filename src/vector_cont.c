#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include "vector_cont.h"


// WIDELY INSPIRED BY
// https://gist.github.com/EmilHernvall/953968/0fef1b1f826a8c3d8cfb74b2915f17d2944ec1d0
//
// Under MIT LICENSE


void vector_init (Vector *v, size_t s)
{
    // TODO free bfr init /!\ Recursivity not _free()?
    v->data    = NULL;
    v->size    = 0;
    v->count   = 0;
    v->el_size = s;
}

int vector_count (Vector *v) { return v->count; }

void vector_add (Vector *v, void *e)
{ // when does not need to malloc
    if (v->size == 0)
    {
        v->size = 10;
        v->data = malloc (sizeof (void *) * v->size * v->el_size);
        memset (v->data, '\0', sizeof (void) * v->size * v->el_size);
    }

    // condition to increase v->data:
    // last slot exhausted
    if (v->size == v->count)
    {
        v->size *= 2; // a strategie
        v->data = realloc (v->data, sizeof (void *) * v->size * v->el_size);
    }

    memcpy (&v->data[v->count * v->el_size], e, v->el_size);
    v->count++;
}

void vector_reserve (Vector *v, int s)
{
    if (v->count >= s) return; // error

    v->size = s;
    v->data = realloc (v->data, sizeof (void *) * v->size * v->el_size);
}

void vector_set (Vector *v, int index, void *e)
{
    if (index >= v->count)
    {
        return;
    }

    v->data[index * v->el_size] = e;
}

void *vector_get (Vector *v, int index)
{
    if (index >= v->count)
    {
        return NULL;
    }

    return &v->data[index * v->el_size];
}

void vector_delete (Vector *v, int index)
{
    if (index >= v->count)
    {
        return;
    }

    for (int i = index; i < v->count; i++)
    {
        memcpy (&v->data[i * v->el_size], &v->data[(i + 1) * v->el_size], v->el_size);
    }

    v->count--;
}

void vector_free (Vector *v)
{
    free (v->data);

    // vector_init(v); DEPRECATED
}

void vector_swap (Vector *v, int i1, int i2)
{
    void *temp = alloca (v->el_size);
    memcpy (temp, vector_get (v, i1), v->el_size);
    memcpy (vector_get (v, i1), vector_get (v, i2), v->el_size);
    memcpy (vector_get (v, i2), temp, v->el_size);
}

#ifdef TEST_VECTOR
#include "io.h"

int main (void)
{
    printUt (0, "Vector (contiguous version)");

    printUt (1, "Init vector");

    Vector v;
    vector_init (&v, 30);


    printUt (1, "A vect with some strings");
    vector_add (&v, "emil");
    vector_add (&v, "hannes");
    vector_add (&v, "lydia");


    printUt (1, "Just a swap");
    vector_swap (&v, 0, 1);

    printUt (1, "Count and get");
    int i;
    printf ("first round:\n");
    for (i = 0; i < vector_count (&v); i++)
    {
        printf ("%s\n", (char *)vector_get (&v, i));
    }


    printUt (1, "Free the vector");
    vector_free (&v);


    printUt (1, "A vect with some integers");
    vector_init (&v, sizeof (int));
    int a = 397;
    vector_add (&v, &a);
    a = 747;
    vector_add (&v, &a);
    a = 815;
    vector_add (&v, &a);
    a = 123;
    vector_add (&v, &a);
    a = 456;
    vector_add (&v, &a);
    a = 789;
    vector_add (&v, &a);
    a = 737;
    vector_add (&v, &a);


    printUt (1, "Deleting elements");
    vector_delete (&v, 5);
    vector_delete (&v, 0);

    for (i = 0; i < vector_count (&v); i++)
    {
        int *out = vector_get (&v, i);
        printf ("%i\n", *out);
    }

    printUt (1, "Reserve some space");
    vector_reserve (&v, 50);

    vector_free (&v);

    printUt (6, "Vector");

    return 0;
}
#endif
