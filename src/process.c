#include "process.h"
#include "futil.h"
#include "io.h"
#include "llist.h"
#include "mutil.h"
#include <bsd/string.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "timeread.h"


Measure line_to_data (char *line, enum e_type select)
{
    // init
    Measure ret = { 0, 0, 0, 0, 0, 0.0, 0.0, -1 };

    // tokenizing
    char *ptr;
    strtok (line, ",");
    if (select & (ozone | all_types)) ret.ozone = atoi (line);

    ptr = strtok (NULL, ",");
    if (select & (particullate_matter | all_types)) ret.particullate_matter = atoi (ptr);

    ptr = strtok (NULL, ",");
    if (select & (carbon_monoxide | all_types)) ret.carbon_monoxide = atoi (ptr);

    ptr = strtok (NULL, ",");
    if (select & (sulfure_dioxide | all_types)) ret.sulfure_dioxide = atoi (ptr);

    ptr = strtok (NULL, ",");
    if (select & (nitrogen_dioxide | all_types)) ret.nitrogen_dioxide = atoi (ptr);

    ptr = strtok (NULL, ",");
    if (select & (type_coord)) ret.longitude = atof (ptr);

    ptr = strtok (NULL, ",");
    if (select & (type_coord)) ret.latitude = atof (ptr);

    // time is easy to deduce from index of line, so not here

    return ret;
}


FDesc read_header (char *filepath)
{
    FILE *f = fopen (filepath, "r"); // TODO: No fclose???

    char s[130];
    fgets (s, 130, f);
    fgets (s, 80, f);
    fclose (f);
    Measure m   = line_to_data (s, type_coord);
    FDesc   ret = { { m.longitude, m.latitude }, "" };
    strlcpy (ret.name, filepath, MAXSIZEFN);
    return ret;
}

void record_all_headers (Vector *filepaths, Dstr *d)
{
    for (int i = 0; i < filepaths->count; i++)
    {
        FDesc f = read_header (vector_get (filepaths, i));
        dstr_register_a_file (d, f.name, f.coor.lat, f.coor.lon);
    }
}


void fill_data (Dstr *d, char *dir)
{
    Vector filepaths;
    vector_init (&filepaths, sizeof (char) * 60);
    list_all_files (&filepaths, dir);
    record_all_headers (&filepaths, d);
    vector_free (&filepaths);
}

// PROCESS COMMAND :


double _radius_to_double (enum e_radius r)
{
    switch (r)
    {
    case rinf:
        exit (-1); // should never happenned
    case r1:
        return 0.00001;
        break;
    case r10:
        return 0.0001;
        break;
    case r100:
        return 0.001;
        break;
    case r1000:
        return 0.01;
        break;
    }
    return 0.0;
}

bool _is_in_bound (FDesc *f, Command c)
{
    // in other version, could be remplaced by real distance calculus
    // if(c.radius == rinf) //Should never happenned
    //  return true;
    double r = _radius_to_double (c.radius);
    // printf ("%f;%f;%f;%f||%f;%f\n", c.pos.lat - r, f->coor.lat, c.pos.lat + r, f->coor.lat, c.pos.lon, f->coor.lon);
    if (c.pos.lon - r < f->coor.lon && c.pos.lon + r > f->coor.lon)
        if (c.pos.lat - r < f->coor.lat && c.pos.lat + r > f->coor.lat) return true;
    // printf ("false\n");
    return false;
}

void reduce_to_concerned (Cell **l, Command c)
{
    Cell *prec = NULL;

    while (*l != NULL)
    {
        if (!_is_in_bound ((*l)->ptr_value, c))
            llist_rm_el (l, prec);
        else
        {
            prec = *l;
            l    = &((**l).next);
        }
    }
}

void get_interested_files (Cell **fds, Dstr *d, Command c)
{ // first rough fds-filling (using dichotomy)
#if NODICHO != 0
  // No dicho
    printfv (1, "(get_interested_files):No Dicho\n");
    for (int i = 0; i < vector_count (&d->filenames); i++)
    {
        FDesc *fd = vector_get (&d->filenames, i);
        llist_add_head (fds, fd); // Dumbely copy entirely
        // printf("%lf\t", fd->coor[0]);DBG ONLY
    }
#else
  // dicho
    printfv (1, "(get_interested_files):w/ Dicho\n");
    // TODO: May choose which axis (according to dist to mean/med)
    // But now only with lat_table
    double radius = _radius_to_double (c.radius);

    Ranged r_coord;
    r_coord.low = c.pos.lat - radius;
    r_coord.high = c.pos.lat + radius;
    printfv (2, "For the dico, segment is %lf to %lf\n", r_coord.low, r_coord.high); // DBGONLY

    Rangei r_in_table;
    r_in_table = ddichotomy (&d->lat_table, r_coord);


    for (int i = r_in_table.low; i < r_in_table.high; i++)
    {
        CoordElement *address = vector_get (&d->lat_table, i);
        FDesc *fd = vector_get (&d->filenames, address->i);
        llist_add_head (fds, fd);
    }

    // printfv (2, "After the dicho, there are %d files interested\n", vector_count (fds)); // DBGONLY

#endif
}


int _measure_to_value (Measure m, enum e_type t)
{
    switch (t)
    {
    case ozone:
        return m.ozone;
    case particullate_matter:
        return m.particullate_matter;
    case carbon_monoxide:
        return m.carbon_monoxide;
    case sulfure_dioxide:
        return m.sulfure_dioxide;
    case nitrogen_dioxide:
        return m.nitrogen_dioxide;
    case all_types:
        exit (-1);
    case type_coord:
        exit (-1);
    }
    return 0;
}

Answer compute_data (Vector *fds, Command c, Rangei t_range)
{
    printfv (1, "(compute_data):\n");
    Answer ans = { 0.0, 0, 0, 0 };

    int n_measure_taken = 0;
    // n_measure_taken     = (t_range.high - t_range.low) * vector_count (fds);
    n_measure_taken = 0; // DBGONLY

    bool first_measure = true;

    for (int i = 0; i < vector_count (fds); i++)
    { // each file
        // printf("i:%d;%s\n", i, ((FDesc*)vector_get(fds, i))->name); //DBGONLY
        // Find data pointer
        FILE *f = fopen (((FDesc *)vector_get (fds, i))->name, "r");
        char  s[130];
        // Jump to data line //ISSUE#13
        for (int j = -1; j < t_range.low; j++) fgets (s, 129, f);
        // printf("%s\n", s);//DBGONLY

        for (int j = t_range.low; j <= t_range.high; j++)
        {
            // TODO: Here, with file reading, should be in Dstr or FUTIL
            fgets (s, 130, f);
            Measure m  = line_to_data (s, c.type);
            int     m2 = _measure_to_value (m, c.type);

            if (first_measure)
            {
                // init for min/max because no +/-inf in C (could with macro)
                ans.max       = m2;
                ans.min       = m2;
                first_measure = false;
            }
            else
            {
                if (ans.max < m2) ans.max = m2;
                if (ans.min > m2) ans.min = m2;
            }
            ans.avg += m2;
            n_measure_taken += 1;
        }
        fclose (f);
    }

    printfv (2, "There is %d measures taken in account\n", n_measure_taken);

    ans.avg = ans.avg / n_measure_taken;
    ans.n   = n_measure_taken; // DBGONLY??
    return ans;
}

void process (Dstr *d, Command c)
{
    printfv (1, "(process):A new command beeing to be processed\n");
    print_command (c);

    Vector *fds;      // means file descriptor : pointer (to disk or ram) w/ coords
    Vector  fds_heap; // because init Wall needed
    if (c.radius != rinf)
    {
        vector_init (&fds_heap, sizeof (FDesc));
        fds = &fds_heap;

        vector_reserve (fds, 20); // Dumb and should be modified, but perf=>up

        // use linkedlist for delete performance
        // then copy to comply with code for the else
        Cell *fds_list = NULL;

        get_interested_files (&fds_list, d, c); // first rough fds-filling (using dichotomy)
        reduce_to_concerned (&fds_list, c);


        // copy to a vector
        for (Cell *curr_cell = fds_list; curr_cell != NULL; curr_cell = curr_cell->next)
            vector_add (fds, curr_cell->ptr_value);
        llist_free (&fds_list);

        printfv (2, "After the copy to vector, there are %d files interested\n", vector_count (fds)); // DBGONLY
    }
    else
    {
        fds = &d->filenames;
    }


    Rangei t_range = get_time_range_from_command (c);

    printfv (2, "(in process):t_range before protection : %d;%d", t_range.low, t_range.high);
    // range protection
    if (t_range.low < 0) t_range.low = 0;
    if (t_range.high > 17567) t_range.high = 17567;

    // printf("t_range:%d;%d\n", t_range.low, t_range.high);//DBGoNLY

    Answer ans;
    ans = compute_data (fds, c, t_range);
    printfv (2, "(in process):free fds\n");
    if (fds != &d->filenames) vector_free (fds);

    print_answer (ans);
}

// END PROCESS COMMAND

#ifdef TEST_PROCESS
#include "io.h"

int main ()
{
    printUt (0, "Process");
    printUt (1, "Process line to data");

    char    line[] = "103,30,56,71,48,10.109984271163967,56.16459887184971,2014-08-01 00:05:00";
    Measure m      = line_to_data (line, all_types);
    printf ("%i,%i,%i,%i,%i,%lf,%lf,%d\n", m.ozone, m.particullate_matter, m.carbon_monoxide,
            m.sulfure_dioxide, m.nitrogen_dioxide, m.longitude, m.latitude, m.timestamp);

    // printf("\n**Process a command**\n");
    // Command c1 = {3, "NULL", 1, {0.0, 0.0}, rinf};

    printUt (6, "Process");
    return 0;
}
#endif
