#include "vector_scat.h"
#include <bsd/string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// WIDELY INSPIRED BY
// https://gist.github.com/EmilHernvall/953968/0fef1b1f826a8c3d8cfb74b2915f17d2944ec1d0
//
// Under MIT LICENSE


void vector_init (Vector *v)
{
    v->data  = NULL;
    v->size  = 0;
    v->count = 0;
}

int vector_count (Vector *v) { return v->count; }

void vector_add (Vector *v, void *e)
{ // when does not need to malloc
    if (v->size == 0)
    {
        v->size = 10;
        v->data = malloc (sizeof (void *) * v->size);
        memset (v->data, '\0', sizeof (void) * v->size);
    }

    // condition to increase v->data:
    // last slot exhausted
    if (v->size == v->count)
    {
        v->size *= 2; // a strategie
        v->data = realloc (v->data, sizeof (void *) * v->size);
    }

    v->data[v->count] = e;
    v->count++;
}


// Special adds type
void vector_add_str (Vector *v, char *e)
{
    char *pt = malloc (sizeof (char) * (strlen (e) + 1));
    strlcpy (pt, e, strlen (e));
    vector_add (v, pt);
}

void vector_add_int (Vector *v, int e)
{
    int *pt = malloc (sizeof (int));
    *pt     = e;
    vector_add (v, pt);
}


void vector_add_alloc (Vector *v, void *e, size_t s)
{
    void *pt = malloc (s);
    memcpy (pt, e, s);
    vector_add (v, pt);
}

void vector_reserve (Vector *v, int s)
{
    if (v->count >= s) return; // error

    v->size = s;
    v->data = realloc (v->data, sizeof (void *) * v->size);
}

void vector_set (Vector *v, int index, void *e)
{
    if (index >= v->count)
    {
        return;
    }

    v->data[index] = e;
}

void *vector_get (Vector *v, int index)
{
    if (index >= v->count)
    {
        return NULL;
    }

    return v->data[index];
}

void vector_delete (Vector *v, int index)
{
    if (index >= v->count)
    {
        return;
    }

    free (v->data[index]); // axel mem
    v->data[index] = NULL;

    int    i, j;
    void **newarr = (void **)malloc (sizeof (void *) * v->count * 2);
    for (i = 0, j = 0; i < v->count; i++)
    {
        if (v->data[i] != NULL)
        {
            newarr[j] = v->data[i];
            j++;
        }
    }

    free (v->data);

    v->data = newarr;
    v->count--;
}

void vector_free (Vector *v)
{
    for (int i = 0; i < vector_count (v); i++)
    {
        free (v->data[i]); // axel mem
    }
    free (v->data);

    vector_init (v);
}

#ifdef TEST_VECTOR
#include "io.h"

int main (void)
{
    printUt (0, "Vector (scattered version)");

    printUt (1, "Init vector");
    Vector v;
    vector_init (&v);


    printUt (1, "Add some strings");
    vector_add_alloc (&v, "emil", (size_t)5);
    vector_add_alloc (&v, "hannes", (size_t)7);
    vector_add_str (&v, "lydia");
    char *str = "olle";
    vector_add_str (&v, str);
    vector_add_alloc (&v, "erik", (size_t)5);

    printUt (1, "Count and get");
    int i;
    printf ("first round:\n");
    for (i = 0; i < vector_count (&v); i++)
    {
        printf ("%s\n", (char *)vector_get (&v, i));
    }

    printUt (1, "Delete some");
    vector_delete (&v, 1);
    vector_delete (&v, 3);

    printUt (1, "Free the vector");
    vector_free (&v);


    printUt (1, "Add some integers");
    vector_add_int (&v, 897);
    vector_add_int (&v, 465);
    vector_add_int (&v, 321);

    for (i = 0; i < vector_count (&v); i++)
    {
        int *out = vector_get (&v, i);
        printf ("%i\n", *out);
    }

    printUt (1, "Add some chaos (heterogeneous)");
    char *c = malloc (sizeof (c));
    *c      = 'b';
    vector_add (&v, c);

    typedef struct a_struct
    {
        int       a;
        char      b;
        struct s *ps;
    } mytype;
    mytype *s = malloc (sizeof (mytype));
    s->a      = 3;
    s->b      = 'r';
    s->ps     = NULL;
    vector_add (&v, s);

    vector_add_str (&v, "Just me");

    char *c2 = vector_get (&v, v.count - 3);
    printf ("char : %c\n", *c2);
    mytype *s2;
    s2 = vector_get (&v, v.count - 2);
    printf ("mytype : s->a:%i s->b:%i s->ps:%p\n", s2->a, s2->b, s2->ps);
    printf ("string : %s\n", (char *)vector_get (&v, v.count - 1));


    printUt (1, "Reserve some space");
    vector_reserve (&v, 50);

    vector_free (&v);
    printUt (6, "Vector");

    return 0;
}
#endif
