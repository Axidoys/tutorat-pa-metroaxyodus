#include "dstr.h"
#include <bsd/string.h>
#include <stdio.h>
#include <stdlib.h>


void dstr_init (Dstr *d)
{
    vector_init (&d->long_table, sizeof (CoordElement));
    vector_init (&d->lat_table, sizeof (CoordElement));
    vector_init (&d->filenames, sizeof (FDesc));
}

void dstr_register_a_file (Dstr *d, char *fn, double lat, double lon)
{
    // cant init inline in C
    Coord c;
    c.lat = lat;
    c.lon = lon;
    FDesc fd;
    fd.coor = c;
    strlcpy (fd.name, fn, MAXSIZEFN);
    int          i      = vector_count (&d->filenames);
    CoordElement ce_lat = { lat, i };
    CoordElement ce_lon = { lon, i };

    // malloc for persistancy
    // FDesc * pt_fd = malloc(sizeof(dstr));
    // CoordElement * pt_ce_lat = malloc(sizeof(ce));
    // CoordElement * pt_ce_lon = malloc(sizeof(ce));


    // assign values
    //*pt_fd = fd;
    //*pt_ce_lat = ce_lat;
    //*pt_ce_lon = ce_lon;

    // Vector add
    vector_add (&d->filenames, &fd);
    vector_add (&d->long_table, &ce_lon);
    vector_add (&d->lat_table, &ce_lat);
}

void dstr_free (Dstr *d)
{
    vector_free (&d->long_table);
    vector_free (&d->lat_table);
    vector_free (&d->filenames);
}

void dstr_print_tables (Dstr *d)
{
    printf ("\n=>Showing long_table :\n");
    for (int i = 0; i < vector_count (&(d->long_table)); i++)
    {
        CoordElement *out = vector_get (&(d->long_table), i);
        printf ("%lf\n", out->c);
    }

    printf ("\n=>Showing lat_table :\n");
    for (int i = 0; i < vector_count (&(d->lat_table)); i++)
    {
        CoordElement *out = vector_get (&(d->lat_table), i);
        printf ("%lf\n", out->c);
    }

    printf ("\n=>Showing filenames :\n");
    for (int i = 0; i < vector_count (&(d->filenames)); i++)
    {
        FDesc *f;
        f = vector_get (&(d->filenames), i);
        printf ("%s\n", f->name);
    }
}

bool qs_cmp_ce (Vector *v, int iLow, int iHigh)
{
    return ((CoordElement *)vector_get (v, iLow))->c < ((CoordElement *)vector_get (v, iHigh))->c;
}

#ifdef TEST_DSTR
#include "io.h"

int main (void)
{
    printUt (0, "dstr");
    printUt (1, "init dstr");
    Dstr d;
    dstr_init (&d);

    printUt (1, "Add some elements");
    dstr_register_a_file (&d, "fn156875", 151.213, 635.642);
    dstr_register_a_file (&d, "fn156876", 158.426, 634.842);
    dstr_register_a_file (&d, "fn156877", 192.742, 633.722);
    dstr_register_a_file (&d, "fn156878", 141.164, 632.62);
    dstr_register_a_file (&d, "fn156879", 100.778, 631.5000);

    dstr_print_tables (&d);
    printUt (1, "Free");
    dstr_free (&d);

    printUt (6, "dstr");

    return 0;
}
#endif
