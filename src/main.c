#include "common.h"
#include "dstr.h"
#include "io.h"
#include "mutil.h"
#include "process.h"
#include <bsd/string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


Command cmd_request (void *instream)
{
    Command cmd = { -1, "", -1, { 0.0, 0.0 }, rinf, begin };

    char arg[MAXCMDSIZE] = "BUGIFAPPEAR";
    // setbuf(stdin, NULL);//not sure https://stackoverflow.com/questions/34219549/how-to-properly-flush-stdin-in-fgets-loop
    printfv (0, "\n>");
    char *retEOF = fgets (arg, MAXCMDSIZE, instream); // TODO change to 'getline'
    popCR (arg);

    // cmd processing
    if (strlen (arg) == 0 || arg[0] == ' ')
    {
        printError ("please type-in something");
        return cmd;
    }
    if (retEOF == NULL)
    {
        //		printf("EOF\n");//DBGONLY
        cmd.state = s_EOF;
        return cmd;
    }

    strtok (arg, " ");


    if (strcmp (arg, "log") == 0)
        cmd.metric = log;
    else if (strcmp (arg, "daily") == 0)
        cmd.metric = daily;
    else if (strcmp (arg, "monthly") == 0)
        cmd.metric = monthly;
    else if (strcmp (arg, "global") == 0)
        cmd.metric = global;
    else
    {
        printError ("wrong arg for metric");
        //		printf("got %s", arg);//DBGONLY
        // TODO can explicit awaited/given
        return cmd;
    }
    char *ptr = strtok (NULL, " ");
    //** OPT **
    if (cmd.metric == log)
    {

        if (ptr == NULL || strlen (ptr) == 0)
        {                                                     // there no more arguments
            printError ("not enough arguments (needs date)"); // need hour;
            return cmd;
        }

        // TODO must check date format
        strlcpy (cmd.opt, ptr, MAXOPTSIZE);

        // update token only in this if bloc
        ptr = strtok (NULL, " ");

        if (ptr == NULL || strlen (ptr) == 0)
        {                                                     // there no more arguments
            printError ("not enough arguments (needs hour)"); // need hour;
            return cmd;
        }

        strlcat (cmd.opt, " ", MAXOPTSIZE);
        strlcat (cmd.opt, ptr, MAXOPTSIZE);

        ptr = strtok (NULL, " ");
    }
    else if (cmd.metric == monthly || cmd.metric == daily)
    {
        if (ptr == NULL || strlen (ptr) == 0)
        {                                                     // there no more arguments
            printError ("not enough arguments (needs date)"); // need hour;
            return cmd;
        }

        // TODO must check date format
        strlcpy (cmd.opt, ptr, MAXOPTSIZE);

        // update token only in this if bloc
        ptr = strtok (NULL, " ");
    }
    else
    {
        strlcpy (cmd.opt, "NULL", MAXOPTSIZE);
    }


    // for type
    if (ptr == NULL || strlen (ptr) == 0)
    { // there no more arguments
        printError ("not enough arguments (need type)");
        return cmd;
    }


    if (strcmp (ptr, "ozone") == 0)
        cmd.type = ozone;
    else if (strcmp (ptr, "particullate_matter") == 0)
        cmd.type = particullate_matter;
    else if (strcmp (ptr, "carbon_monoxide") == 0)
        cmd.type = carbon_monoxide;
    else if (strcmp (ptr, "sulfure_dioxide") == 0)
        cmd.type = sulfure_dioxide;
    else if (strcmp (ptr, "nitrogen_dioxide") == 0)
        cmd.type = nitrogen_dioxide;
    else
    {
        printError ("wrong arg for type");
        // TODO can explicit awaited/given
        return cmd;
    }

    // update token only in this if bloc
    ptr = strtok (NULL, " ");

    if (ptr != NULL && strlen (ptr) != 0)
    { // there is no more argument
        // position filter
        cmd.pos.lon = strtod (ptr, NULL);
        ptr         = strtok (NULL, " ");

        cmd.pos.lat = strtod (ptr, NULL);
        ptr         = strtok (NULL, " ");

        if (strcmp (ptr, "1") == 0)
            cmd.radius = r1;
        else if (strcmp (ptr, "10") == 0)
            cmd.radius = r10;
        else if (strcmp (ptr, "100") == 0)
            cmd.radius = r100;
        else if (strcmp (ptr, "1000") == 0)
            cmd.radius = r1000;
        else
        {
            printError ("wrong arg for radius");
            // TODO: can explicit awaited/given
            return cmd;
        }
    }
    else
        cmd.radius = rinf;

    cmd.state = valid;

    return cmd;
}

int main (int argc, char *argv[])
{

    if (argc < 2 || argc > 3)
    {
        printError ("number of args invalid");
        printHelpRun (argv[0]);
        return 1;
    }

    if (argc == 2 && strcmp (argv[1], "-h") == 0)
    {
        printHelpRun (argv[0]);
        return 0;
    }


    void *instream;

    if (argc == 2)
    {
        // interactive mode
        printfv (0, "\ninteractive mode launched :\n");
        printfv (0, "enter your commands, \'h\' to get help :\n");
        instream = stdin;
    }
    else // if (argc == 3)
    {
        // command file create check
        // mode batch
        printfv (0, "\nbatch mode\n\n");
        instream = fopen (argv[2], "r"); // TODO: change
    }

    // format data path
    pushSLASH (argv[1]);

    // read files
    Dstr data;
    dstr_init (&data);
    fill_data (&data, argv[1]);

    if (vector_count (&data.long_table) == 0)
    {
        printError ("no files scanned");
        printfv (0, "program launched but useless\n");
    }
    else
        printfv (0, "%d files scanned\n", vector_count (&data.long_table));

    quickSort (&data.lat_table, 0, vector_count (&data.lat_table) - 1);
    // quickSort (&data.long_table, 0, vector_count (&data.long_table) - 1);


    // dstr_print_tables(&data);//DBGONLY

    Command cmd;
    do
    {

        do
        {
            cmd = cmd_request (instream);
        } while (cmd.state == begin);

        if (cmd.state != s_EOF) process (&data, cmd);

    } while (cmd.state != s_EOF); // arg[0]!=EOF but automatic


    dstr_free (&data);
    fclose (instream); // no problem, even if stdin
    printf ("Thanks you using me !\n");


    return 0;
}
