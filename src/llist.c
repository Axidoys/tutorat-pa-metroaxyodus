#include "llist.h"
#include <stdlib.h>


void llist_add_head (Cell **list, void *p_v)
{
    // take only pointer
    // so pay attention to persistency of data
    Cell *newCell      = malloc (sizeof (Cell));
    newCell->next      = *list;
    newCell->ptr_value = p_v;

    *list = newCell;
}


void llist_rm_el (Cell **c_to_rm, Cell *c_prec)
{
    Cell *temp_free_ptr = *c_to_rm;

    if (c_prec != NULL) // not rm the head
        c_prec->next = (*c_to_rm)->next;
    else // rm the head
        *c_to_rm = (*c_to_rm)->next;

    free (temp_free_ptr);
}

void llist_free (Cell **list)
{
    Cell *curr_cell = *list;
    *list           = NULL;

    while (curr_cell != NULL)
    {
        Cell *to_free_cell = curr_cell;
        curr_cell          = curr_cell->next;
        free (to_free_cell);
    }
}


#ifdef TEST_LLIST
#include "io.h"

void print_ints (Cell *list)
{
    if (list == NULL) return;
    do
    {
        printf ("%d\n", *(int *)list->ptr_value);
        list = list->next;
    } while (list != NULL);
}

int main ()
{
    printUt (0, "llist");

    printUt (1, "Init list with _add_head");
    Cell *l;
    int   v1 = 13;
    llist_add_head (&l, &v1);

    print_ints (l);


    printUt (1, "Add 4 more els");
    int v2 = 22;
    llist_add_head (&l, &v2);
    int v3 = 33;
    llist_add_head (&l, &v3);
    int v4 = 44;
    llist_add_head (&l, &v4);
    int v5 = 55;
    llist_add_head (&l, &v5);

    print_ints (l);


    printUt (1, "Rm the 2nd el");
    llist_rm_el (&l->next, l);

    print_ints (l);


    printUt (1, "Rm the first el");
    llist_rm_el (&l, NULL);

    print_ints (l);


    printUt (1, "Rm the last () el");
    llist_rm_el (&l, NULL);
    llist_rm_el (&l, NULL);
    llist_rm_el (&l, NULL);


    printUt (1, "free");
    llist_add_head (&l, &v2);
    llist_add_head (&l, &v3);
    llist_add_head (&l, &v4);
    llist_add_head (&l, &v5);

    llist_free (&l);

    print_ints (l);

    printUt (6, "llist");
}

#endif
