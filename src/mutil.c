#include "mutil.h"

// TODO Refactor iHigh, high, etc.

#include <stdio.h> //DBGONLY


// inspired from https://www.frasq.org/en/manual/c-in-7-days/search-by-dichotomy
int dichotomy (Vector *v, Rangei r, double target)
{
    int i;
    while (r.low <= r.high)
    {
        i = (r.high + r.low) / 2;
        // if (tab[ i ] == val) assume that double does not verify strict equalities
        //    return i;
        if (((CoordElement *)vector_get (v, i))->c < target)
        {
            r.low = i + 1;
            i += 1;
        }
        else
            r.high = i - 1;
    }
    return i; // i-0.5 is the targeted place
}

Rangei ddichotomy (Vector *v, Ranged target)
{
    // suppose that Ranged is ordonned (r.low<r.high)
    Rangei r = { 0, vector_count (v) - 1 };
    int    i = -1;
    while (r.low <= r.high)
    {
        i = (r.high + r.low) / 2;

        if (((CoordElement *)vector_get (v, i))->c < target.low)
            r.low = i + 1;
        else if (((CoordElement *)vector_get (v, i))->c > target.high)
            r.high = i - 1;
        else
        { // dicho diverge for two different dicho // cannot optimize anymore
            int temprlow = dichotomy (v, r, target.low);
            r.high       = dichotomy (v, r, target.high);
            r.low        = temprlow;
            return r;
        }
    }
    r.low  = i;
    r.high = i;
    return r; // i-0.5 is the targeted place/range
}


// private function
int _partition (Vector *v, int low, int high)
{
    double pivot = ((CoordElement *)vector_get (v, high))->c; // pivot
    int    i     = (low - 1);                                 // Index of smaller element

    for (int j = low; j <= high - 1; j++)
    {
        // If current element is smaller than the pivot
        if (((CoordElement *)vector_get (v, j))->c < pivot)
        // TODO use qs_cmp and pivot => pi (not its value, only index)
        {
            i++; // increment index of smaller element
            vector_swap (v, i, j);
        }
    }
    vector_swap (v, i + 1, high);
    return (i + 1);
}

// adapted from https://www.geeksforgeeks.org/quick-sort/
void quickSort (Vector *v, int iLow, int iHigh)
{
    if (iLow < iHigh)
    {
        /* pi is partitioning index, arr[p] is now
           at right place */
        int pi = _partition (v, iLow, iHigh);

        // Separately sort elements before
        // partition and after partition
        quickSort (v, iLow, pi - 1);
        quickSort (v, pi + 1, iHigh);
    }
}

#ifdef TEST_MUTIL
#include "dstr.h"
#include "io.h"

int main ()
{
    printUt (0, "Mutil");
    printUt (1, "Quick sort test");

    Dstr d;
    dstr_init (&d);
    dstr_register_a_file (&d, "fn156875", 154.213, 639.642);
    dstr_register_a_file (&d, "fn156875", 154.213, 638.642);
    dstr_register_a_file (&d, "fn156875", 154.213, 637.642);
    dstr_register_a_file (&d, "fn156875", 154.213, 636.642);
    dstr_register_a_file (&d, "fn156875", 154.213, 635.642);
    dstr_register_a_file (&d, "fn156876", 158.426, 634.842);
    dstr_register_a_file (&d, "fn156877", 192.742, 633.722);
    dstr_register_a_file (&d, "fn156878", 141.164, 632.62);
    dstr_register_a_file (&d, "fn156879", 100.778, 631.5000);


    quickSort (&d.long_table, 0, vector_count (&d.long_table) - 1);
    quickSort (&d.lat_table, 0, vector_count (&d.lat_table) - 1);

    dstr_print_tables (&d);


    printUt (1, "Dichotomy");
    Rangei r = { 0, vector_count (&d.long_table) - 1 };
    printf ("%d:%d:%d:%d\n", dichotomy (&d.long_table, r, 630.0), dichotomy (&d.long_table, r, 640.0),
            dichotomy (&d.long_table, r, 633.0), dichotomy (&d.long_table, r, 636.0));

    printUt (1, "Double dichotomy");
    Rangei rout;
    Ranged t = { 633.0, 637.0 };
    rout     = ddichotomy (&d.long_table, t);
    printf ("(%d;%d)\n", rout.low, rout.high);


    dstr_free (&d);

    printUt (6, "Mutil");

    return 0;
}
#endif
