#include "io.h"
#include <stdarg.h>

void popCR (char *str)
{
    while (str[0] != '\0' && str[0] != '\n')
    {
        str = &str[1];
    }
    str[0] = '\0';
}

void pushSLASH (char *str)
{
    int i = 0;
    for (i = 0; i < MAXSIZEFN && str[i] != '\0'; i++)
    {
    }

    if (i >= MAXSIZEFN - 1) return; // no placed

    if (str[i - 1] == '/') return; // already formated

    str[i + 1] = '\0';
    str[i]     = '/';
}

void print_command (Command c)
{
    printfv (2, "metric %d ; date '%s' ; type %d ; Coord (%4f, %4f) ; radius %d\n", c.metric, c.opt,
             c.type, c.pos.lat, c.pos.lon, c.radius);
}

void print_answer (Answer a)
{
    if (VERBOSE_THRESHOLD != -1)
    { // NORMAL
        if (a.n == 0)
            printError ("No measure, the filter is too restrictive");
        else
            printfv (0, "%lf %i %i\n", a.avg, a.min, a.max);
    }
    else
    { // CSV
        if (a.n == 0)
            printfv (-1, "None\n");
        else
            printfv (-1, "%lf\t%i\t%i\n", a.avg, a.min, a.max);
    }
}

void printError (char *errStr)
{ // do not show error in csv mode
    printfv (0, RED "Error :" YEL " '%s'\n" RESET, errStr);
}

void printHelpRun (char *prgName)
{
    printfv (0, GRN "Help :\n" RESET);
    printfv (0, "  Analyse the data of amtospheric sensors placed in Aarhus (Denmark)\n");
    printfv (0, "  They are two modes\n");
    printfv (0, "   * interactive mode\n");
    printfv (0, "    The user can manually run commands, one after the other\n");
    printfv (0, "    Enter it by typing :");
    printfv (0, "        %s <data dir path>\n", prgName);
    printfv (0, "   * batch mode\n");
    printfv (0, "    A list of commands written in a text file is executed\n");
    printfv (0, "    Enter it by typing :");
    printfv (0, "        %s <data dir path> <text-commands file>\n", prgName);
    printfv (0, "\n");
    printfv (0, "  If you want help for commands syntax, type in:\n");
    printfv (0, "    %s -h\n", prgName);
}

void printHelpCommand (char *prgName)
{
    printfv (0, GRN "  Style of commands :\n" RESET);
    printfv (0, "  Usage:\n");
    printfv (0, "  %s <metric> [opt] <type> [longitude latitude radius]\n", prgName);
}

void printUt (int lvl, char *str)
{
    /*
      0 : name of the run
      1 : name of what is being processed
      2 : subtitle for precision
      3 : informal details //may not be use?
      4 : normally out/expected
      5 : content
      6 : Finish Statement
    */
    if (lvl == 0)
    {
        printf (BLU "---- Running UT for %s -----\n" RESET, str);
    }
    else if (lvl == 1)
    {
        printf (CYN "** Test of : %s **\n" RESET, str);
    }
    else if (lvl == 4)
    {
        printf ("(Expected : " GRN "%s" RESET " )\n", str);
    }
    else if (lvl == 6)
    {
        printf (BLU "---- Finish UT for %s -----\n" RESET, str);
    }
    else
    {
        printf ("%s\n", str);
    }
}

void printfv (int v, char *s, ...)
{
    va_list args;
    va_start (args, s);

    if (v == -1 && VERBOSE_THRESHOLD == -1) // CSV mode
    {
        // pay attention that CSV and other modes are disjoint
        // NOLINTNEXTLINE(clang-analyzer-valist.Uninitialized)
        vprintf (s, args);
    }
    else if (v == 0 && VERBOSE_THRESHOLD >= 0) // RELEASE
                                               // NOLINTNEXTLINE(clang-analyzer-valist.Uninitialized)
        vprintf (s, args);
    else if (v == 1 && VERBOSE_THRESHOLD >= 1)
    {
        char buffer[MAXCMDSIZE]; // could be an over name but same value make sense (i<=>o)
        // NOLINTNEXTLINE(clang-analyzer-valist.Uninitialized)
        vsprintf (buffer, s, args);
        printf ("%s %s %s", CYN, buffer, RESET);
    }
    else if (v == 2 && VERBOSE_THRESHOLD >= 2)
    {
        char buffer[MAXCMDSIZE];
        // NOLINTNEXTLINE(clang-analyzer-valist.Uninitialized)
        vsprintf (buffer, s, args);
        printf ("%s %s %s", BLU, buffer, RESET);
    }

    va_end (args);
}
