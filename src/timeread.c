#include "timeread.h"

#define __USE_XOPEN // for time.h
#include <stdint.h> //for time
#include <time.h>   //To include in macro??


int _timeread_month (char *cp)
{ // it is 8, 9 or 0 (for 10)
    if (cp[1] == '8') return 0;
    if (cp[1] == '9') return 8928;  // 31days divided per 5minutes
    if (cp[1] == '0') return 17568; // 61 days divided per 5minutes

    return 0; // Normally error, do we have to exit()?
}

int _timeread_day (char *cp)
{
    int ret = 0;

    // DAY tens
    if (cp[0] == '0')
        ret -= 288; //???
    else if (cp[0] == '1')
        ret += 2592; // 10days divided by 5min
    else if (cp[0] == '2')
        ret += 5472; // 20days divided by 5min
    else if (cp[0] == '3')
        ret += 8064; // 30days divided by 5min
    // Not a classical timestamp under
    /*
     */


    // DAY units
    // if =='0' : implicit
    if (cp[1] == '1')
        ret += 288;
    else if (cp[1] == '2')
        ret += 576;
    else if (cp[1] == '3')
        ret += 864;
    else if (cp[1] == '4')
        ret += 1152;
    else if (cp[1] == '5')
        ret += 1440;
    else if (cp[1] == '6')
        ret += 1728;
    else if (cp[1] == '7')
        ret += 2016;
    else if (cp[1] == '8')
        ret += 2304;
    else if (cp[1] == '9')
        ret += 2592;

    // if(cp[0] != '0')//because we begin day count at dd=01
    //  ret += 288;
    // but moved below
    // ret -= 288;

    return ret;
}

int _timeread_clock (char *cp) // another name? for hour-minute reading
{                              // not using strtoi for perfs
    int ret = 0;

    // HOUR tens
    // if =='0' : implicit
    if (cp[0] == '1')
        ret += 120; // 10h divided by 5minutes
    else if (cp[0] == '2')
        ret += 240; // 20h divided by 5minutes

    // HOUR units
    // if =='0' : implicit
    if (cp[1] == '1')
        ret += 12; // 1h divided by 5min
    else if (cp[1] == '2')
        ret += 24; // 2h divided by 5min
    else if (cp[1] == '3')
        ret += 36; // 3h divided by 5min
    else if (cp[1] == '4')
        ret += 48; // 4h divided by 5min
    else if (cp[1] == '5')
        ret += 60; // 5h divided by 5min
    else if (cp[1] == '6')
        ret += 72; // 6h divided by 5min
    else if (cp[1] == '7')
        ret += 84; // 7h divided by 5min
    else if (cp[1] == '8')
        ret += 96; // 8h divided by 5min
    else if (cp[1] == '9')
        ret += 108; // 9h divided by 5min

    // MINUTES tens
    // if == '0' : implicit
    if (cp[3] == '1')
        ret += 2; // 10min divided by 5min
    else if (cp[3] == '2')
        ret += 4; // 20min divided by 5min
    else if (cp[3] == '3')
        ret += 6; // 30min divided by 5min
    else if (cp[3] == '4')
        ret += 8; // 40min divided by 5min
    else if (cp[3] == '5')
        ret += 10; // 50min divided by 5min
    // Not a classical timestamp under
    /*
    else if(cp[3] == '6')
      ret += 12;
    else if(cp[3] == '7')
      ret += 14;
    else if(cp[3] == '8')
      ret += 16;
    else if(cp[3] == '9')
      ret += 18;
    */

    // MINUTES units
    // if == '0' : implicit
    if (cp[4] == '5') ret += 1; // 5min divided by 5min

    return ret;
}

Rangei get_time_range_from_command1 (Command c)
{
    Rangei t_range;
    t_range.low  = 0;
    t_range.high = 17568;

    if (c.metric == global)
    {
    }
    else if (c.metric == monthly)
    {
        t_range.low += _timeread_month (&c.opt[5]);

        t_range.high = t_range.low + 8928; //+One month -1
                                           // TODO: See if months = 30/31days
    }
    else if (c.metric == daily)
    {
        t_range.low += _timeread_month (&c.opt[5]);
        t_range.low += _timeread_day (&c.opt[8]);

        t_range.high = t_range.low + 288; //+One day -1
    }
    else if (c.metric == log)
    {
        t_range.low += _timeread_month (&c.opt[5]);
        t_range.low += _timeread_day (&c.opt[8]);
        t_range.low += _timeread_clock (&c.opt[11]);

        t_range.high = t_range.low + 0;
    }


    return t_range;
}

Rangei get_time_range_from_command2 (Command c)
{
    Rangei t_range;

    if (c.metric == global)
    {
        t_range.low  = 0;
        t_range.high = 17568;
        return t_range;
    }


    struct tm tm_begin = { 0, 0, 0, 1, 8, 114, 0, 0, 0, 0, 0 }; // needed init for warnings
    struct tm tm_end;

    if (c.metric == monthly)
    {
        strptime (c.opt, "%Y-%m", &tm_begin);
        tm_end = tm_begin;
        tm_end.tm_mon += 1;
    }
    else if (c.metric == daily)
    {
        strptime (c.opt, "%Y-%m-%d", &tm_begin);
        tm_end = tm_begin;
        tm_end.tm_mday += 1;
    }
    else if (c.metric == log)
    {
        strptime (c.opt, "%Y-%m-%d %H:%M:%S", &tm_begin);
        tm_end = tm_begin;
    }

    time_t t_begin = mktime (&tm_begin);
    // printf("[%jd]\n", (intmax_t)t_begin);
    // printf("[%.f]\n", difftime((time_t)0, t_begin));
    time_t t_end = mktime (&tm_end);
    // offset to new epoch
    time_t t_offset = 1406847600;
    t_begin -= t_offset; // TODO: USE DIFFTIME?
    t_end -= t_offset;   // TODO: USE DIFFTIME?
    // get only /5minutes
    t_range.low  = t_begin / 300; // 300s = 5min
    t_range.high = t_end / 300;   // 300s = 5min

    // recover index
    // t_range.low -=1;
    // t_range.high -= 1;


    // printf("[%d]\n", t_range.low);

    return t_range;
}


Rangei get_time_range_from_command (Command c)
{
    // put differentiation in a unique function for clarity
#if SPECIFICTIME != 0
    return get_time_range_from_command1 (c);
#else
    return get_time_range_from_command2 (c);
#endif
}
