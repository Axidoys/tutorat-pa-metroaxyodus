#include "io.h"
#include "vector_cont.h"
#include <bsd/string.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXFNSIZE 20

void list_all_files (Vector *v, char *path)
{
    struct dirent *lecture;
    DIR *          rep;
    rep = opendir (path);

    /*fichier inexistant*/
    if (rep == NULL)
    {
        printError ("dir not exist\n");
        exit (1);
    };
    // TODO May be getting number of files for vector_reserve()

    int i = v->count; //=0 normally
    while ((lecture = readdir (rep)))
    {
        // TODO dont forget to doc it
        if (lecture->d_name[0] != '.') // because of ., .. or .files
        {
            vector_add (v, path);
            strlcat (vector_get (v, i), lecture->d_name, MAXSIZEFN);

            i++;
        }
    }

    free (rep);
    int closedir (DIR * repertoire);
}


int print_all_files (char *path) // DBG ONLY and should be remplaced by vector_str_print
{
    struct dirent *lecture;
    DIR *          rep;
    rep = opendir (path); // chemin temporaire pour test

    /*fichier inexistant*/
    if (rep == NULL)
    {
        printError ("dir not exist\n");
        exit (1);
    };

    while ((lecture = readdir (rep)))
    {
        printf ("- nom du fichier : %s\n", lecture->d_name);
    }

    int closedir (DIR * repertoire);
    return 0;
}


#ifdef TEST_FUTIL
#include "io.h"

int main ()
{
    printUt (0, "FUTIL");


    printUt (1, "print root files");
    print_all_files ("./");

    printUt (1, "record root files");
    Vector v;
    vector_init (&v, sizeof (char) * MAXFNSIZE);
    list_all_files (&v, "./");
    printf ("Vector size is %i el\n", v.count); // TODO: kept printf for format...
    printf ("the first element is : %s\n", (char *)vector_get (&v, 0));

    printUt (6, "FUTIL");
    return 0;
}
#endif
