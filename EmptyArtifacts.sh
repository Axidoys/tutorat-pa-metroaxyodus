#!/bin/bash
token=INSERT A PERSONNAL ACCES TOKEN HERE // SHOULD PASS IT WITH USER
echo "using personal token ${token}"

project_id="17849448" # project_id, find it here: https://gitlab.com/[organization name]/[repository name] at the top underneath repository name

#token="YOUR_TOKEN" # your personal access token
server="gitlab.com" # gitlab.com or gitlab.example.in

# Retrieving Jobs list page count
total_pages=$(curl -sD - -o /dev/null -X GET \
  "https://$server/api/v4/projects/$project_id/jobs?per_page=100" \
  -H "PRIVATE-TOKEN: ${token}" | grep -Fi X-Total-Pages | sed 's/[^0-9]*//g')

# Creating list of Job IDs for the Project specified with Artifacts
job_ids=()
echo ""
echo "Creating list of all Jobs that currently have Artifacts..."
echo "Total Pages: ${total_pages}"
for ((i=2;i<=${total_pages};i++)) #starting with page 2 skipping most recent 100 Jobs
do
  echo "Processing Page: ${i}/${total_pages}"
  response=$(curl -s -X GET \
    "https://$server/api/v4/projects/$project_id/jobs?per_page=100&page=${i}" \
    -H "PRIVATE-TOKEN: ${token}")
  length=$(echo $response | jq '. | length')
  for ((j=0;j<${length};j++))
  do
    if [[ $(echo $response | jq ".[${j}].artifacts_file | length") > 0 ]]; then
        echo "Job found: $(echo $response | jq ".[${j}].id")"
        job_ids+=($(echo $response | jq ".[${j}].id"))
    fi
  done
done

# Loop through each Job erasing the Artifact(s)
echo ""
echo "${#job_ids[@]} Jobs found. Commencing removal of Artifacts..."
for job_id in ${job_ids[@]};
do
  response=$(curl -s -X DELETE \
    -H "PRIVATE-TOKEN:${token}" \
    "https://$server/api/v4/projects/$project_id/jobs/$job_id/artifacts")
  echo "Processing Job ID: ${job_id} - Status: $(echo $response | jq '.status')"
done
