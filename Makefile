BIN = bin
OBJ = obj
SRC = src
INCLUDE = include
CFLAGS =
CLIBS = -lbsd
GNUMAKEFLAGS = --no-print-directory
DSTR?=1
VECT?=1
FUTIL?=1
PROCESS?=1
MUTIL?=1
LLIST?=1

VERBOSE_THRESHOLD?=3

RELEASE?=0
ifeq ($(RELEASE), 1)
    CFLAGS = -W -Wall
		VERBOSE_THRESHOLD = 0
else
    CFLAGS = -W -Wall -g -O0
endif

STRAT_NODICHO?=0
STRAT_SPECIFICTIME?=1

all: compile libs ut

compile: obj bin prgm

libs: obj ${OBJ}/vector_scat.o ${OBJ}/dstr.o ${OBJ}/main.o ${OBJ}/vector_cont.o ${OBJ}/futil.o ${OBJ}/process.o ${OBJ}/mutil.o ${OBJ}/io.o

ut: ut_futil ut_process ut_dstr ut_vector_scat ut_vector_cont ut_mutil ut_llist

ut_llist: obj bin
	make ut_llist.out LLIST=1

ut_futil: obj bin
	make ut_futil.out FUTIL=1

ut_process: obj bin
	make ut_process.out PROCESS=1

ut_mutil: obj bin
	make ut_mutil.out MUTIL=1

ut_dstr: obj bin
	make ut_dstr.out DSTR=1

ut_vector_scat : obj bin
	make ut_vector_scat.out VECT=1

ut_vector_cont : obj bin
	make ut_vector_cont.out VECT=1


prgm : ${OBJ}/dstr.o ${OBJ}/main.o ${OBJ}/vector_cont.o ${OBJ}/futil.o ${OBJ}/process.o ${OBJ}/mutil.o ${OBJ}/io.o ${OBJ}/timeread.o ${OBJ}/llist.o
	gcc -o ${BIN}/$@.out $^ ${CFLAGS} ${CLIBS}


ut_llist.out : ${OBJ}/test_llist.o ${OBJ}/io.o
	gcc -o ${BIN}/$@ $^ ${CFLAGS} ${CLIBS}

ut_futil.out : ${OBJ}/test_futil.o ${OBJ}/io.o ${OBJ}/vector_cont.o
	gcc -o ${BIN}/$@ $^ ${CFLAGS} ${CLIBS}

ut_process.out : ${OBJ}/test_process.o ${OBJ}/io.o ${OBJ}/vector_cont.o ${OBJ}/dstr.o ${OBJ}/futil.o ${OBJ}/mutil.o ${OBJ}/timeread.o ${OBJ}/llist.o
	gcc -o ${BIN}/$@ $^ ${CFLAGS} ${CLIBS}

ut_mutil.out : ${OBJ}/test_mutil.o ${OBJ}/io.o ${OBJ}/dstr.o ${OBJ}/vector_cont.o
	gcc -o ${BIN}/$@ $^ ${CFLAGS} ${CLIBS}

ut_dstr.out : ${OBJ}/test_dstr.o ${OBJ}/io.o ${OBJ}/vector_cont.o
	gcc -o ${BIN}/$@ $^ ${CFLAGS} ${CLIBS}

ut_vector_scat.out : ${OBJ}/test_vector_scat.o ${OBJ}/io.o
	gcc -o ${BIN}/$@ $^ ${CFLAGS} ${CLIBS}

ut_vector_cont.out : ${OBJ}/test_vector_cont.o ${OBJ}/io.o
	gcc -o ${BIN}/$@ $^ ${CFLAGS} ${CLIBS}


${OBJ}/llist.o : ${SRC}/llist.c
	gcc -I ${INCLUDE} -o $@ -c $^ ${CFLAGS}

${OBJ}/timeread.o : ${SRC}/timeread.c
	gcc -I ${INCLUDE} -DSPECIFICTIME=${STRAT_SPECIFICTIME} -o $@ -c $^ ${CFLAGS}

${OBJ}/io.o : ${SRC}/io.c
	gcc -I ${INCLUDE} -DVERBOSE_THRESHOLD=${VERBOSE_THRESHOLD} -o $@ -c $^ ${CFLAGS}

${OBJ}/futil.o : ${SRC}/futil.c
	gcc -I ${INCLUDE} -o $@ -c $^ ${CFLAGS}

${OBJ}/process.o : ${SRC}/process.c
	gcc -I ${INCLUDE} -DNODICHO=${STRAT_NODICHO} -o $@ -c $^ ${CFLAGS}

${OBJ}/mutil.o : ${SRC}/mutil.c
	gcc -I ${INCLUDE} -o $@ -c $^ ${CFLAGS}

${OBJ}/vector_scat.o : ${SRC}/vector_scat.c
	gcc -I ${INCLUDE} -o $@ -c $^ ${CFLAGS}

${OBJ}/vector_cont.o : ${SRC}/vector_cont.c
	gcc -I ${INCLUDE} -o $@ -c $^ ${CFLAGS}

${OBJ}/dstr.o : ${SRC}/dstr.c
	gcc -I ${INCLUDE} -o $@ -c $^ ${CFLAGS}

${OBJ}/test_llist.o : ${SRC}/llist.c
	gcc -I ${INCLUDE} -DTEST_LLIST -o $@ -c $^ ${CFLAGS}

${OBJ}/test_futil.o : ${SRC}/futil.c
	gcc -I ${INCLUDE} -DTEST_FUTIL -o $@ -c $^ ${CFLAGS}

${OBJ}/test_process.o : ${SRC}/process.c
	gcc -I ${INCLUDE} -DTEST_PROCESS=0 -o $@ -c $^ ${CFLAGS}

${OBJ}/test_mutil.o : ${SRC}/mutil.c
	gcc -I ${INCLUDE} -DTEST_MUTIL -o $@ -c $^ ${CFLAGS}

${OBJ}/test_vector_scat.o : ${SRC}/vector_scat.c
	gcc -I ${INCLUDE} -DTEST_VECTOR -o $@ -c $^ ${CFLAGS}

${OBJ}/test_vector_cont.o : ${SRC}/vector_cont.c
	gcc -I ${INCLUDE} -DTEST_VECTOR -o $@ -c $^ ${CFLAGS}

${OBJ}/test_dstr.o : ${SRC}/dstr.c
	gcc -I ${INCLUDE} -DTEST_DSTR -o $@ -c $^ ${CFLAGS}

${OBJ}/main.o : ${SRC}/main.c
	gcc -I ${INCLUDE} -o $@ -c $^ ${CFLAGS}


obj :
	@-mkdir ${OBJ}

bin :
	@-mkdir ${BIN}

clean :
	rm -rf obj/*.o

mrproper: clean
	rm -rf bin
	rm -rf obj
