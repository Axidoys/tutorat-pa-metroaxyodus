import os
import sys

#do not deserve to be used by a format check
#do not check format but hypothesis based on the format defined in README.md
#for example, this version of the script bug if there is not enough column


def bToS(b):
    if(b):
        return "True"
    else:
        return "False"


path = "../data/pollutionzip/"
if(len(sys.argv)==2) :
    path = sys.argv[1]

if(os.path.isdir(path)):

    #flags of hypothesis
    f_consTags = True
    f_timeBeginAt = None
    f_timeBeginSame = True
    f_timeJumpFive = True
    f_NmeasuresSame = True



    sameTime = None
    Nmeasures = None


    if(True): #TODO check for zero file dir
        fileName = ""
        for r, d, f in os.walk(path):
            for fileName in f:
                #Parsing files
                os.path.join(r, fileName) + "\n"
                file = open(os.path.join(r, fileName))



                line1 = file.readline()
                if(line1 != "ozone,particullate_matter,carbon_monoxide,sulfure_dioxide,nitrogen_dioxide,longitude,latitude,timestamp\n"):
                    f_consTags = False

                line2 = file.readline()
                if(sameTime==None):#first time
                    sameTime=line2.split(',')[7]
                else:
                    if(sameTime!=line2.split(',')[7]):
                        f_timeBeginSame=False

                n = 1

                lineprec = line2
                linenext = None
                for linenext in file.readlines():
                    # Parsing lines
                    n+=1

                    #TODO time jump 5

                    lineprec = linenext

                if (Nmeasures == None):  # first time
                    Nmeasures = n
                else:
                    if (Nmeasures != n):
                        f_NmeasuresSame = False

        if(f_timeBeginSame and sameTime=="2014-08-01 00:05:00\n"):
            f_timeBeginAt = True
        else:
            f_timeBeginAt = False


        print("End of analyse")

        print("** Hypothesis : ")
        print(" - The columns are like they would be : ", bToS(f_consTags))
        print(" - All the files begin at the same time : ", bToS(f_timeBeginSame))
        if(f_timeBeginSame):
            print("   + All the files begin at 2014-08-01 00:05:00 : ", bToS(f_timeBeginAt))
        print(" - [WIP]Line jump = 5min : ", bToS(f_timeJumpFive))
        print(" - There is always the same number of measure per file : ", bToS(f_NmeasuresSame))
        if(f_NmeasuresSame):
            print("   + All the files have this number of measure : ", Nmeasures)

    else:
        print("No files in dir")

else:
    print("Folder do not exist")
