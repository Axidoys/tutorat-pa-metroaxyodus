import matplotlib.pyplot as plt
import os
import sys


path = "../data/pollutionzip/"
if(len(sys.argv)==2) :
    path = sys.argv[1]

if(os.path.isdir(path)):

    if(True): #TODO check for zero file dir
        fileName = ""
        x=[]
        y=[]
        co=[]
        for r, d, f in os.walk(path):
            for fileName in f:
                os.path.join(r, fileName) + "\n"
                file = open(os.path.join(r, fileName))
                file.readline()
                line = file.readline().split(",")
                xPos = float(line[5])
                yPos = float(line[6])

                if not(xPos in x and yPos in y):
                    x+=[xPos]
                    y+=[yPos]
                    co+=[len(x)]


        print("len = ", len(x))

        plt.scatter(x, y, c=co, cmap='inferno')
        plt.colorbar()
        plt.xlabel('long')
        plt.ylabel('lat')
        plt.show()

    else:
        print("No files in dir")

else:
    print("Folder do not exist")
