import sys, os
from subprocess import call

formal_types = ["feat", "fix", "docs", "style", "refactor", "test", "chore"]
formal_scopes = ["main", "io", "common", "futil", "mutil", "process", "vector", "dstr"]
formal_scopes += ["all", "git", "CI/CD", "Makefile", "timetest", "analyse", "doc", "delivery"]

def exitWithError(errorstr):
	print("\x1B[31mcommit aborted\x1B[0m, there is an error : " + errorstr)
	sys.exit(2)


message_file = sys.argv[1]
f = open(message_file, "r")
lines = f.readlines()

warnings = []


#search for format : "type(scope): subject"
if len(lines[0].split(':')) != 2 :
	exitWithError( "no subject, format is : type(scope): subject" )
parts = lines[0].split(':')

if len(parts[0].split('(')) != 2 :
	exitWithError( "no scope, format is : type(scope): subject" )
parts = parts[0].split('(') + [parts[1]]

if parts[1][len(parts[1])-1] != ')': #check for closing parenthesis
	warnings += ["no closing parenthesis"]
else:
	parts[1] = parts[1][0:len(parts[1])-1]#DBGONLY see if works

s_type = parts[0]
s_scope = parts[1]
s_subject = parts[2]

#check for type
if not s_type in formal_types:
	exitWithError( "invalid type" )

#check for scope
if not s_scope in formal_scopes:
	warnings += ["the scope is unlisted"]

#check len for subject
if len(s_subject)>50 : #too long
	exitWithError( "subject is too long")
if len(s_subject)>30 : #too long
	warnings += ["subject may be too long"]

#check Space & Case for subject
if len(s_subject)>0 and s_subject[0]!=" ":
	warnings += ["missing a space before subject"]

#check if 2nd line exist, that it is blank
if len(lines) > 1 and lines[1]!= "\n":
	print(lines[1])
	warnings += ["no blank line before body"]



if len(warnings) > 0:
	print("\x1B[33mCommit paused\x1B[0m, there are " +  str(len(warnings)) + " warnings")
	for msg in warnings:
		print("** " + msg)

	answer = raw_input("Type in yes or else : \n>")
	#Why python2 is installed on my machines?
	print("Wow")
	if(answer[0]=='y' or answer[0]=='Y'):
		print("decided to use this message after warnings")
	else:
		exitWithError("aborted after warnings")

print("linter for message \x1B[32mpassed\x1B[0m")

sys.exit(0)
