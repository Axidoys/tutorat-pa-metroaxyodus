import os
import sys
import random
import datetime
import time


#GENERAL options
NOSECOND = True


#options to pick up
cmd_metric=["global", "monthly", "daily", "log"]
weight_metric=[1, 5, 5, 3]
cmd_date=[]
cmd_type=["ozone", "particullate_matter", "carbon_monoxide", "sulfure_dioxide", "nitrogen_dioxide"]
## no weight
cmd_coord=[0, 0]
#gaussian
cmd_radius=["1000", "100", "10", "1"]
#no weight between these values

#consts
LAT_MIN=56.07
LAT_MAX=56.25
LAT_MEAN= ( LAT_MAX+LAT_MIN )/2
lat_3sigma= ( LAT_MAX-LAT_MIN )/2 #just temp var
LAT_SIGMA=lat_3sigma/3

LON_MIN=10.05
LON_MAX=10.28
LON_MEAN= ( LON_MAX+LON_MIN )/2
lon_3sigma= ( LON_MAX-LON_MIN )/2 #just temp var
LON_SIGMA=lon_3sigma/3

DATE_MIN=1406847600
DATE_MAX=1412204400

COORD_PRECISION=8


def cleanseq(l): #remove zero-char string from list
    while "" in l:
        l.remove("")

#because OOP missed me
class Cmd :
    m_metric = ""
    m_date = ""
    m_type = ""
    m_coord = ""
    m_radius = ""

    m_string_to_pass = ""

#    def __init__(self):

    def generate(self):
        self.m_metric = random.choices(cmd_metric, weights=weight_metric)[0]

        if(self.m_metric == "global"):
            self.m_date = ""
        else:
            #need date

            d = random.randint(DATE_MIN, DATE_MAX)

            if(self.m_metric == "monthly"):
                self.m_date = time.strftime("%Y-%m", time.gmtime(d))
            elif(self.m_metric == "daily"):
                self.m_date = time.strftime("%Y-%m-%d", time.gmtime(d))
            else: # if(m_metric == "log"):
                if(NOSECOND):
                    self.m_date = time.strftime("%Y-%m-%d %H:%M:00", time.gmtime(d))
                else:
                    self.m_date = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(d))

        self.m_type = random.choice(cmd_type)

        if(random.choices([True, False], [90, 10])[0]): #use a geo filter / p=
            lon=random.gauss(LON_MEAN, LON_SIGMA)
            lat=random.gauss(LAT_MEAN, LAT_SIGMA)
            self.m_coord = " ".join([str(lon)[0:COORD_PRECISION], str(lat)[0:COORD_PRECISION]])

            self.m_radius = random.choices(cmd_radius)[0]
        else:
            self.m_coord = ""
            self.m_radius = ""


    def print_command(self):
        seq = [self.m_metric, self.m_date, self.m_type, self.m_coord, self.m_radius]
        cleanseq(seq)

        print(" ".join(seq))


#input is NUMBER
path = ""
if(not len(sys.argv)==2) :
    print("no enough args")
    exit(1)

c = Cmd()
for i in range(int(sys.argv[1])):
    c.generate()
    c.print_command()
