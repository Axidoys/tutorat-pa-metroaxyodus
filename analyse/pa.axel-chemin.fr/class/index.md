# Checking our answers

In the aim of checking the answers produced by our projects, here is how we can compare our returned values


## Get the sample file

There are three files hosted, `cmdX.txt` where `X` is the number of lines.

You can find theses files at :
* cmd5.txt : [basic](http://pa.axel-chemin.fr/class/cmd5.txt)
* cmd50.txt : [comparative](http://pa.axel-chemin.fr/class/cmd50.txt)
* cmd200.txt : [perfomance tests](http://pa.axel-chemin.fr/class/cmd200.txt)

## Produce your data

Assuming that your program works fine, you can do these steps only in command line :
```
cd myRepo
wget http://pa.axel-chemin.fr/class/cmd50.txt
bin/myProgram.out DataFolderFromJDequidt < cmd50.txt > data_ans.txt
```

In order to have a clean data file, you should pay attention what you print, don't use "debug printing" and use the comma ',' (like csv) as separator between values

## Publish your answers

Now, because data_ans.txt is in csv format, you can just copy the content and paste it on the gdoc [here](./gd/)

As we chose, the ready to use, Gdoc, it is more convenience to use the `cmd50.txt`


PS. this docs is convert to pdf using : https://markdowntohtml.com/
