<html>

<head>
	<title>Line Chart</title>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
	<script src="./utils.js"></script>

	<script>
	var z_dates = [];
	var z_times = [];
	var z_values = [];
	</script>

	<?php
	$txt_file    = file_get_contents('data.txt');
	$rows        = explode("\n", $txt_file);
	array_shift($rows);

	foreach($rows as $row => $data)
	{
		//get row data
		$row_data = explode(';', $data);

		$info[$row]['date']         = $row_data[0];
		$info[$row]['time']         = $row_data[1];

		//display data
		//date
		echo '<script>z_dates.push( ';
		echo $info[$row]['date'];
		echo ' );</script>' ;
		//time
		echo '<script>z_times.push( ';
		echo $info[$row]['time'];
		echo ' );</script>' ;

		//both
		echo '<script>z_values.push( ' ;
		echo '{x: ' . $info[$row]['date'];
		echo ', y:' . $info[$row]['time'];
		echo '} ); </script>' ;

	}
	?>

<script>
	console.log(z_dates);
	console.log(z_times);
	console.log(z_values);
</script>




	<style>
	canvas{
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
	</style>
</head>

<body>

	<div style="width:75%;">
		<canvas id="canvas"></canvas>
	</div>
	<br>
	<br>
	<button id="interpolation1">Default interpolation (because it's good to see)</button>
	<button id="interpolation2">Monotone interpolation (a little more scientific)</button>
	<button id="draw1">Straigth line (it's getting cold)</button>
	<button id="draw2">Only steps (no lies about unmeasured values)</button>
	<button id="draw3">Only points (no lies about unmeasured values)</button>

	<script>
		var config = {
			type: 'line',
			data: {
				datasets: [{
					label: 'dataset',
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					data: z_values,
					fill: false,
				}]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: 'Speed of execution of CmetroAxYo-dus'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
                type: 'linear',
                position: 'bottom'
            }],
					x: {
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Date'
						}
					},
					y: {
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Running time'
						}
					}
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);
		};


document.getElementById('interpolation1').addEventListener('click', function() {
	window.myLine.options.elements.line.tension = 0.4;
	window.myLine.options.elements.line.borderWidth = 3;//please, it is not elegant, but it keeps the animation
	window.myLine.options.elements.line.stepped = false;
	window.myLine.options.elements.line.cubicInterpolationMode = 'default';
	window.myLine.update();
});
document.getElementById('interpolation2').addEventListener('click', function() {
	//window.myLine.options.elements.line.tension = 0.4;
	window.myLine.options.elements.line.borderWidth = 3;//please, it is not elegant, but it keeps the animation
	window.myLine.options.elements.line.stepped = false;
	window.myLine.options.elements.line.cubicInterpolationMode = 'monotone';
	window.myLine.update();
});
document.getElementById('draw1').addEventListener('click', function() {
	window.myLine.options.elements.line.tension = 0.0000001;
	window.myLine.options.elements.line.borderWidth = 3;//please, it is not elegant, but it keeps the animation
	window.myLine.options.elements.line.stepped = false;
	window.myLine.options.elements.line.cubicInterpolationMode = 'default';
	window.myLine.update();
});
document.getElementById('draw2').addEventListener('click', function() {
	//window.myLine.options.elements.line.tension = 0.0000001;
	window.myLine.options.elements.line.borderWidth = 3;//please, it is not elegant, but it keeps the animation
	window.myLine.options.elements.line.stepped = true;
	//window.myLine.options.elements.line.cubicInterpolationMode = 'default';
	window.myLine.update();
});
document.getElementById('draw3').addEventListener('click', function() {
	//window.myLine.options.elements.line.tension = 0.0000001;
	window.myLine.options.elements.line.borderWidth = 0.0000001;//please, it is not elegant, but it keeps the animation
	//window.myLine.options.elements.line.stepped = true;
	//window.myLine.options.elements.line.cubicInterpolationMode = 'default';
	window.myLine.update();
});




	</script>
</body>

</html>
