#include "dstr.h"
#include "vector_cont.h"
#include <stdio.h>


int main ()
{
    printf ("**Run test for data allocation with contiguous structure**\n");
    printf ("**W _reserve()**\n");

    Vector v;
    vector_init (&v, sizeof (Coord));
    vector_reserve (&v, 88123123);

    Coord c = { 135.0543, 88652133.15 };
    for (int i = 0; i < 88123123; i++)
    {
        vector_add (&v, &c);
    }

    return 0;
}
