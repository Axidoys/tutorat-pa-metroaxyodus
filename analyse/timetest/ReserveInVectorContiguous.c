#include "dstr.h"
#include "vector_cont.h"
#include <stdio.h>


int main ()
{
    printf ("**Run test for data allocation with contiguous structure**\n");
    printf ("**W _reserve()**\n");

    Vector v;
    vector_init (&v, sizeof (coord));
    vector_reserve (&v, 20000000);

    Coord c = { 135.0543, 88652133.15 };
    for (int i = 0; i < 123456; i++)
    {
        Vector v;
        vector_init (&v, sizeof (coord));
        vector_reserve (&v, 20000000);
        vector_free (&v);
    }

    return 0;
}
