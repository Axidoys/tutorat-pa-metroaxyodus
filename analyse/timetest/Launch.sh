make $1

NUMBERRUN=${NUMBERRUN:=2}

a=-1
while [ $a -lt $NUMBERRUN ]
do
   time $1.out
   a=`expr $a + 1`
done
