#include "dstr.h"
#include "vector_scat.h"
#include <stdio.h>


int main ()
{
    printf ("**Run test for data allocation with scaterred structure**\n");
    printf ("**No _reserve()**\n");

    Vector v;
    vector_init (&v);

    Coord c = { 135.0543, 88652133.15 };
    for (int i = 0; i < 20000000; i++)
    {
        vector_add_alloc (&v, &c, sizeof (Coord));
    }

    return 0;
}
