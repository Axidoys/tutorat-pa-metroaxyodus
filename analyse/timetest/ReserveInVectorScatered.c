#include "dstr.h"
#include "vector_scat.h"
#include <stdio.h>


int main ()
{
    printf ("**Run test for data allocation with scaterred structure**\n");
    printf ("**W _reserve()**\n");


    Coord c = { 135.0543, 88652133.15 };
    for (int i = 0; i < 123456; i++)
    {
        Vector v;
        vector_init (&v);
        vector_reserve (&v, 20000000);
        vector_free (&v);
    }

    return 0;
}
