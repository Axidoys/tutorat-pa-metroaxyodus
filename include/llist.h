#ifndef LLIST_H_
#define LLIST_H_

// scattered

typedef struct cell
{
    void *       ptr_value;
    struct cell *next;
} Cell;

void llist_add_head (Cell **list, void *p_v); // faster than add in queu
// void add_to_queu(Cell * list, void* p_v); //slower, no need
void llist_rm_el (Cell **c_to_rm, Cell *c_prec);
void llist_free (Cell **list);

#endif
