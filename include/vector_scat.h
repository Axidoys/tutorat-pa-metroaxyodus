#ifndef VECTORSCAT_H__
#define VECTORSCAT_H__

#include <stddef.h>

// WIDELY INSPIRED BY
// https://gist.github.com/EmilHernvall/953968/0fef1b1f826a8c3d8cfb74b2915f17d2944ec1d0
//
// Under MIT LICENSE


typedef struct vector
{
    void **data;
    int    size;
    int    count;
} Vector;

void  vector_init (Vector *);
int   vector_count (Vector *);
void  vector_add (Vector *, void *);
void  vector_add_str (Vector *, char *);
void  vector_add_int (Vector *, int);
void  vector_add_alloc (Vector *, void *, size_t);
void  vector_reserve (Vector *, int); // take place for further add
void  vector_set (Vector *, int, void *);
void *vector_get (Vector *, int);
void  vector_delete (Vector *, int);
void  vector_free (Vector *);

#endif
