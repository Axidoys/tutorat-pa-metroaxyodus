#ifndef VECTORSCAT_H__
#define VECTORSCAT_H__

#include <stddef.h> //for size_t

// WIDELY INSPIRED BY
// https://gist.github.com/EmilHernvall/953968/0fef1b1f826a8c3d8cfb74b2915f17d2944ec1d0
//
// Under MIT LICENSE


typedef struct vector
{
    void **data;
    int    size;
    int    count;
    size_t el_size;
} Vector;

void vector_init (Vector *, size_t);
int  vector_count (Vector *);
void vector_add (Vector *, void *);
// void vector_add_str(Vector *, char *);  obsolete in scattered
// void vector_add_int(vector*, int);      obsolete in scattered
// void vector_add_alloc(Vector *, void *, size_t); become _add (no more need malloc)
void  vector_reserve (Vector *, int); // take place for further add
void  vector_set (Vector *, int, void *);
void *vector_get (Vector *, int);
void  vector_delete (Vector *, int);
void  vector_swap (Vector *, int, int);
void  vector_free (Vector *);

#endif
