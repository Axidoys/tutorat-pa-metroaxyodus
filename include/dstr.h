#ifndef DSTR_H_
#define DSTR_H_

#include "common.h"
#include "vector_cont.h"
#include <stdbool.h>
#include <string.h>


typedef struct
{
    Coord coor;
    char  name[MAXSIZEFN];
} FDesc; // file descriptor
// DEPRECATED for storage optimization
// Should use : min,max,avg,filename

typedef struct
{
    double c;   // Coord (lat or long)
    int    i;   // index to filenames
} CoordElement; // Coord element

typedef struct
{ // TODO : d'abord les même Vector w/ FDesc??
    Vector long_table;
    // composed of (longitude;int)=ce
    // sort asc by longitude
    Vector lat_table;
    // composed of (latitude;int)=ce
    // sort asc by latitude
    Vector filenames;
    // Vector of (longitude;latitude;filename)
} Dstr;
/*
dstr members are public in this project
because methods would be redundant (long&lat)
  and the place of sort etc. are in other files

Only the add, init, free is here
The swap is in Vector (for _table)
No need of modify, delete

getter and setter would go against lisibility
*/

void dstr_init (Dstr *);

void dstr_register_a_file (Dstr *, char *, double, double);

void dstr_free (Dstr *);


void dstr_print_tables (Dstr *); // DBGONLY


bool qs_cmp_ce (Vector *, int, int);


#endif
