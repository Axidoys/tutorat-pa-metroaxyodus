#ifndef PROCESS_H_
#define PROCESS_H_

#include "common.h"
#include "dstr.h"


Measure line_to_data (char *, enum e_type);

void  read_all_headers (Vector *, Dstr *d);
FDesc read_header (char *filepath);

void fill_data (Dstr *d, char *dir);

// should be private?
// void get_interested_files (Vector *, Dstr *, Command); // first rough fds-filling (using
// dichotomy) void reduce_to_concerned (Vector *, Command); Answer compute_data (Vector *, Command,
// Rangei); end :"private?"
void process (Dstr *, Command);

#endif
