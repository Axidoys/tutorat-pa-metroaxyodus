#ifndef COMMON_H_
#define COMMON_H_

#define MAXOPTSIZE 20
#define MAXSIZEFN 70

// HEADER ONLY
// maths
typedef struct
{
    double low;
    double high;
} Ranged; // double

// Process and maths
typedef struct
{
    int low;
    int high;
} Rangei; // index or integer

// Process or here?
typedef struct
{
    double avg;
    int    min;
    int    max;

    int n; // number of measure DBGONLY??
} Answer;

// From Dstr
typedef struct
{
    double lon;
    double lat;
} Coord;

// Commands
enum e_metric
{
    log,
    daily,
    monthly,
    global
};
enum e_type
{
    ozone               = 1,
    particullate_matter = 2,
    carbon_monoxide     = 4,
    sulfure_dioxide     = 8,
    nitrogen_dioxide    = 16,
    all_types           = 32,

    type_coord = 64
};
enum e_state
{
    begin,
    valid,
    s_EOF
};
enum e_radius
{
    r1,
    r10,
    r100,
    r1000,
    rinf
};

typedef struct
{
    enum e_metric metric;
    char          opt[MAXOPTSIZE];
    enum e_type   type;

    Coord         pos;
    enum e_radius radius;

    enum e_state state; // begin,valid,success
} Command;              // TODO refactor with caps


//?
typedef struct
{
    int    ozone;
    int    particullate_matter;
    int    carbon_monoxide;
    int    sulfure_dioxide;
    int    nitrogen_dioxide;
    double longitude;
    double latitude;
    int    timestamp; // identified by index of line
} Measure;


#endif
