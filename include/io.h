#ifndef IO_H_
#define IO_H_

#include "common.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

#define MAXCMDSIZE 150
#define DATESIZE 30

#ifndef VERBOSE_THRESHOLD
#define VERBOSE_THRESHOLD 3
#endif
//

void popCR (char *str);
void pushSLASH (char *str);
void print_command (Command c);
void print_answer (Answer a);
void printError (char *errStr);
void printHelpRun (char *prgName);
void printHelpCommand (char *prgName);
void printUt (int lvl, char *str);
void printfv (int, char *, ...); // printf with verbose
// 0 = release //or printf directly
// 1 = light print
// 2 = deep in process
// 3 temporary Debug?

#endif
